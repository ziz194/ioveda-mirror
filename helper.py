import datetime
import calendar

def add_months(sourcedate, months):
    ''' add n months to datetime object '''
    
    month = sourcedate.month - 1 + months
    year = sourcedate.year + month / 12
    month = month % 12 + 1
    day = min(sourcedate.day,calendar.monthrange(year,month)[1])
    return datetime.date(year,month,day)
    
    
def n_months_from_now(n):
    return add_months(datetime.datetime.utcnow(), n)
    
















     
     
