import pandas as pd
import numpy as np 


def to_in_w_nan(x):
    if not x:
        z = int(x)
    else:
        z = None

        

train = pd.read_csv('train.csv', dtype=object)
train = train.dropna(subset=['ICD9'])
train = train.dropna(subset=['RFV'])


scf = pd.read_csv('scf.csv', dtype=object)
#scf.index = scf.index.values + 1
scf.index = scf['#'].values
#scf = scf.drop('#',axis=1)
scf = scf[['Category','Factory']]



J = {'S1':'',
     'S2':'',
     'S3':'',
     'W1':'',
     'W2':'',
     'W3':'',
     'P1':'',
     'P2':'',
     'P3':'',
     'DIAG':''}
     
     
D = {}

def get_catgory_factor_pair(list_of_ix):
    Dall = {}
    for ix in list_of_ix:
        D={}
        D[scf.loc[ix].to_dict()['Category']] = scf.loc[ix].to_dict()['Factory']
        Dall.update(D)
    return Dall
    

# limit to case
train = train[train.disease_case_group == '1']
train = train[['ICD9','RFV','disease_case_group','Factor1','Factor2','Factor3','Factor4','SW']]

import random
import requests
import json
headers = {'Accept': 'text/plain', 'Content-type': 'application/json'}
requests.post('http://me.medlanes.com:9999/FEEEEDMEEEEE', data=json.dumps(J))




X = {}
for ICD9 in train.ICD9.unique():   
    J = {'S1':'','S2':'','S3':'','W1':1,'W2':1,'W3':1,'P1':np.nan,'P2':np.nan,
         'P3':np.nan,'DIAG':ICD9,'DIAG1':'','DIAG2':'','COMP_DIAG':'training'}          
    J['ticket'] = str(random.randint(1,1000000))
    for S,P,W,RFV in zip(['S1','S2','S3'],
                         ['P1','P2','P3'],
                         ['W1','W2','W3'],train[train.ICD9 == ICD9].RFV.unique()):                             
        J[S] = RFV
        list_of_factors = train[(train.ICD9 == ICD9) & (train.RFV == RFV)][['Factor1','Factor2','Factor3','Factor4']].dropna(axis=1).values[0].tolist()         
                                                
        if list_of_factors:                                                                            
            J[P] = get_catgory_factor_pair(list_of_factors)
#        else:
#            J[P] = None
        weights = train[(train.ICD9 == ICD9) & (train.RFV == RFV)]['SW'].dropna().values.tolist()
        if weights:
            weights = int(weights[0])
#        else:
#            weights = 1
        J[W] = weights
    r = requests.post('http://me.medlanes.com:9999/FEEEEDMEEEEE', data=json.dumps(J)) 
    print r.text       
    
        




    

     
     
     
     
     