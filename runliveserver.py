from cherrypy import wsgiserver
from ioveda import app
from FlaskConfig import LiveConfig

app.config.from_object(LiveConfig)
d = wsgiserver.WSGIPathInfoDispatcher({'/': app})
server = wsgiserver.CherryPyWSGIServer(('0.0.0.0', 80), d)


if __name__ == '__main__':
   try:
      server.start()
   except KeyboardInterrupt:
      server.stop()

