from helper import n_months_from_now



class Plan():
    payments_params = {'product_type':'',
                       'price':'',
                       'description':'',
                       'total':'',
                       'paypal':''
                       }
                       
    def __init__(self, config, cancel_url='', return_url=''):
        for key in config:
            setattr(self, key, config[key])
            
        self.cancel_url = cancel_url
        self.return_url = return_url
                                  
        if self.currency is 'EUR':
            self.price_template = '{price}'+ u"\u20AC" 
            self.price_str = str(self.price) + u"\u20AC"
        else:          
            self.price_template = '$'+'{price}'
            self.price_str = '$'+str(self.price) 
               
        if not self.one_time:
            self.price_total = self.price_template.format(price=str(self.price*12))
        else:
            self.price_total = self.price_template.format(price=str(self.price))
                    
        if not self.one_time:
            self.price_str = self.price_str + '/Mo'
            self.price_template = self.price_template + '/Mo'

            
    def plans_page_params(self):
        title = self.title
        price = self.price_str
        subs_type = self.subs_type
        whats_in_1 = self.whats_in_1
        whats_in_2 = self.whats_in_2
        payments_url = 'payments?plan={ix}'.format(ix=str(self.ix))
        return locals()
        
    def payments_page_params(self):
        pp = self.payments_params
        pp['product'] = self.title
        pp['price'] = self.price_str 
        pp['total'] = self.price_total 
          
        if self.one_time:
            pp['description'] = 'one time payment'           
        else:
            pp['description'] = 'month to month payment'
    
        pp['paypal'] = "/payments/paypal/plan/"+str(self.ix)
        pp['plan'] = str(self.ix)
        return pp 
    
    
    def paypal_payment_config(self):
        if self.one_time:
            price = self.price
        else:
            price = 12 * self.price
       
        paypal_config = {       
          "intent": "sale",
          "payer": {
            "payment_method": "paypal" },
          "redirect_urls": {
            "return_url": self.return_url,
            "cancel_url": self.cancel_url},
        
          "transactions": [ {
            "amount": {
              "total": str(price),
              "currency": self.currency },
            "description": self.description } ] }
        return paypal_config 
    
    def subscription_untill(self):
        return n_months_from_now(self.subscription_untill)

plan1_config = {'ix':'1',
                'title':'Ask a Question',
                'price':24,
                'currency':'USD',
                'subs_type':'One time consultation',
                'whats_in_1':'Ask a question',
                'whats_in_2':'Get two followups',
                'one_time':True,
                'description':'onetime_ayurveda_consultation',
                'stripe_plan_name':'',
                'subscription_untill':1
                
}   



plan2_config = {'ix':'2',
                'title':'Ayurveda Life',
                'price':19,
                'currency':'USD',
                'subs_type':'Yearly subscription',
                'whats_in_1':'Ask unlimited questions',
                'whats_in_2':'Get personalised Meditation Podcasts',
                'one_time':False,
                'stripe_plan_name':'ioveda_standard',                
                'description':'ayurveda_life',
                'subscription_untill':12
                
}   
 

plan3_config = {'ix':'3',
                'title':'Ayurveda Awesome',
                'price':49,
                'currency':'USD',
                'subs_type':'Yearly subscription',
                'whats_in_1':'Everything in <b>Ayurveda Life</b>',
                'whats_in_2':'Get monthly a wellness basket',
                'one_time':False,
                'stripe_plan_name':'ioveda_premium',                
                'description':'ayurveda_life_pro',
                'subscription_untill':12
                
}   






 
 
 
 
        
        
        
            
        

        
        
        
               
