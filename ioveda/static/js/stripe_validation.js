(function( $, Stripe ) {

	Stripe.setPublishableKey('pk_test_hurj275JKC1oMxATj7P8UQgg');

	var Form = function( element, settings ) {
		this.$element 		= $( element );
		this.settings 		= settings;
		this.$formBtn 		= this.$element.find('#payment-button');
		this.number 		= this.$element.find('[data-stripe="number"]');
		this.cvc 			= this.$element.find('[data-stripe="cvc"]');
		this.month 			= this.$element.find('[data-stripe="month"]');
		this.year 			= this.$element.find('[data-stripe="year"]');

		this.watchEvents();
	};

	Form.prototype.stripeResponseHandler = function( status, response ) {
		if (response.error) {
			// Call function to display errors
			this.showErrors( response.error );
			// Make button clickable again
			this.$formBtn.prop('disabled', false);
		} else {
			// token contains id, last4, and card type
			var token = response.id;
			// Insert the token into the form so it gets submitted to the server
			this.$element.append($('<input type="hidden" name="stripeToken" />').val(token));
			// and re-submit
			this.$element.get(0).submit();
		}
	};

	Form.prototype.showErrors = function( error ) {
		var $errContainer = this.$element.find('.payment-errors');
		var inputArr = [ this.number, this.cvc, this.month, this.year ];

		// show the error message
    	$errContainer.text( error.message );

    	// remove error classes
    	$.each(inputArr, function( index, val ) {
    		val.removeClass('input-error');
    	});

    	// decide where to set error class
    	switch ( error.code ){
    		case 'invalid_number':
	    		this.number.addClass('input-error');
	    		break;
	    	case 'incorrect_number':
	    		this.number.addClass('input-error');
	    		break;
    		case 'invalid_expiry_month':
	    		this.month.addClass('input-error');
	    		break;
    		case 'invalid_expiry_year':
	    		this.year.addClass('input-error');
	    		break;
    		case 'invalid_cvc':
    			this.cvc.addClass('input-error');
    			break;
			case 'incorrect_cvc':
    			this.cvc.addClass('input-error');
    			break;
    	};
    	// print error to console for debugging
    	console.log(error)
	};

	Form.prototype.addInputSpace = function( e ) {
		var target = e.target;
		var position = target.selectionEnd;
		var length = target.value.length;

		// Allow only numbers and put space every 5th letter
		target.value = target.value.replace(/[^\d]/g, '').replace(/(.{4})/g, '$1 ').trim();
		// Setting position to avoid jumping to the end of the string
		target.selectionEnd = position += (
			(target.value.charAt(position - 1) === ' ' && target.value.charAt(length - 1) === ' ' && length !== target.value.length) ? 1 : 0);
	};

	Form.prototype.watchEvents = function() {
		var self = this;

		// Process with payment on form submit
		self.$element.on('submit', function (e) {
			// disable the button to prevent double clicks
			self.$formBtn.prop('disabled', true);

			// Create the card token
			Stripe.card.createToken({
				number: self.number.val(),
				cvc: self.cvc.val(),
				exp_month: self.month.val(),
				exp_year: self.year.val()
			}, self.stripeResponseHandler.bind(self) );

			// disable the from from submitting
			return false;
		});

		// Add space bars when user types the number
		self.number.on('input', function ( e ) {
			self.addInputSpace( e );
		});
	};

	// instantiating form object
	var form = new Form( '#payment-form', Form.settings );

})( jQuery, Stripe );