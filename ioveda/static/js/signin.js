// // Login
// $(document).on('ready',function(){
// $('#signin').on('click',function(e){
// var username = $('#username').val() ;
// var password = $('#password').val() ;
//  $.ajax({
//    type: "POST",
//    url: "login",
//    data: "username="+username+"&password="+password,
//    success: function(response) {
//    jsonResponse = JSON.parse(response);
//    if (jsonResponse.success == true) {
//     window.location.href =  'index'
// //    alert(jsonResponse.redirect)
// //    alert(response);
//     }
// }
// })

// // e.preventDefault() ;
// return false ;
// });
// });

(function( $ ) {
	var username = $('.login-form #username');
	var password = $('.login-form #password');
	var loginButton = $('.login-form #sign-in');

	loginButton.on('click', function( e ) {
		e.preventDefault();

		$.ajax({
			type: 'POST',
			url: 'login',
			data: 'username=' + username.val() + '&password=' + password.val(),
			success: function( response ) {
				console.log(response)
				var jsonResponse = JSON.parse( response );

				if ( jsonResponse.success === true ) {
					window.location.href = 'index';
				};
			}
		});
	})

})( jQuery );