(function() {
	// Instantiate the Object with id of the button and the class that you wish to add
	var WaitingButon = {
		constructor: function( buttonId, buttonClass ) {
			this._buttonId = buttonId;
			this._class = ' ' + buttonClass;
			this.ele = document.getElementById(this._buttonId);
			// check if the button with chosen id is on the page
			if( this.ele !== null ) {
				this.ele.onclick = this.clickAction.bind(this);
			} else {
				console.log('Please make sure that there is a button with this id');
			};
		},

		clickAction: function(e) {
			var list = document.getElementsByClassName('payment-list');
			var input = list[0].getElementsByTagName('input');
			var result;

			for ( var i = 0, l = input.length ; i < l ; i++ ) {
				if ( input[i].checked === true ) {
					result = input[i];
				}
			};

			if ( result !== undefined ) {
				this.ele.className += this._class;
				this.ele.setAttribute('disabled', 'disabled');

				location.href = result.getAttribute('data-payment');
			};
		}
	}
	window.waitingButton = function( buttonId, buttonClass ) {
		var button = Object.create(WaitingButon);
		button.constructor( buttonId, buttonClass );
		return button;
	};
})();