( function( $ ) {

  var questions = $('.faq-question');
  var answers = $('.faq-answer')

  questions.on('click', function(e) {
    var $this = $(this);
    var elementToToggle = $this.siblings();
    var elementsToHide = answers.not(elementToToggle);

    elementToToggle.slideToggle(100);
    elementsToHide.slideUp(100);
  });

})( jQuery );