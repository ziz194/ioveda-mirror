import os



from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask.ext.mandrill import Mandrill
from flask.ext.cors import CORS
from flask.ext.babel import Babel
from flask.ext.geoip import GeoIP
from flask.ext.social.datastore import SQLAlchemyConnectionDatastore
from flask.ext.wtf.csrf import CsrfProtect
# from flask_analytics import Analytics


from itsdangerous import URLSafeSerializer
from flask_oauth import OAuth

from FlaskConfig import LiveConfig , DevelopementConfig, Config
import paypalrestsdk
import stripe
from plans import Plan, plan1_config, plan2_config, plan3_config


basedir = os.path.abspath(os.path.dirname(__file__))

app = Flask(__name__, static_url_path='/static')
cors = CORS(app)
babel = Babel(app)

app.config.from_object(Config)

stripe.api_key = app.config['STRIPE_SECRET_KEY']
                            
db = SQLAlchemy(app)
mandrill = Mandrill(app)
csrf = CsrfProtect(app)
safe = URLSafeSerializer('randomstring12345')

# Configure authentication
login_manager = LoginManager()
login_manager.session_protection = "strong"
login_manager.login_view = "login"
login_manager.init_app(app)

cancel_url = app.config['PAYPAL_CANCEL_URL']
return_url = app.config['PAYPAL_RETURN_URL']



plan1 = Plan(config=plan1_config,
             cancel_url=cancel_url,
             return_url=return_url)
             
plan2 = Plan(config=plan2_config,
             cancel_url=cancel_url,
             return_url=return_url)
             
plan3 = Plan(config=plan3_config,
             cancel_url=cancel_url,
             return_url=return_url)
                          


# Analytics(app)

oauth = OAuth()
facebook = oauth.remote_app('facebook',
    base_url='https://graph.facebook.com/',
    request_token_url=None,
    access_token_url='/oauth/access_token',
    authorize_url='https://www.facebook.com/dialog/oauth',
    consumer_key='818034521606441',
    consumer_secret='3d43b29d984b0439f39daf6181a7a550',
    request_token_params={'scope': 'email'}
)

google = oauth.remote_app('google',
                          base_url='https://www.google.com/accounts/',
                          authorize_url='https://accounts.google.com/o/oauth2/auth',
                          request_token_url=None,
                          request_token_params={'scope': 'https://www.googleapis.com/auth/userinfo.email',
                                                'response_type': 'code'},
                          access_token_url='https://accounts.google.com/o/oauth2/token',
                          access_token_method='POST',
                          access_token_params={'grant_type': 'authorization_code'},
                          consumer_key='873465958598-2qukrr37dh6cj5n8hetk5ne52f54jq6b.apps.googleusercontent.com',
                          consumer_secret='KWcd2nxt1JMCE0-BJaQDe3G1')
                          



paypal_config = paypalrestsdk.configure({
  'mode': app.config['PAYPAL_MODE'],
  'client_id': app.config['PAYPAL_CLIENT_ID'],
  'client_secret': app.config['PAYPAL_CLIENT_SECRET']
})


app.config['GEOIP_FILEPATH'] = 'GeoIP.dat'
app.config['GEOIP_CACHE'] = 'MEMORY_CACHE'



geoip = GeoIP()
geoip.init_app(app)



    
    
    



import models
import views

