"""
This file has been automatically generated with workbench_alchemy v0.2.3
For more details please check here:
https://github.com/PiTiLeZarD/workbench_alchemy
"""

import os
from sqlalchemy.orm import relationship
from sqlalchemy import Column, ForeignKey, create_engine
from sqlalchemy.ext.declarative import declarative_base

if os.environ.get('DB_TYPE', 'MySQL') == 'MySQL':
    from sqlalchemy.dialects.mysql import INTEGER, VARCHAR, DATETIME
else:
    from sqlalchemy import DateTime as DATETIME, Integer, String as VARCHAR

    class INTEGER(Integer):
        def __init__(self, *args, **kwargs):
            super(Integer, self).__init__()


DECLARATIVE_BASE = declarative_base()

engine = create_engine('sqlite')
class Emr(DECLARATIVE_BASE):

    __tablename__ = 'EMR'
    __table_args__ = (
        {'mysql_engine': 'InnoDB', 'sqlite_autoincrement': True, 'mysql_charset': 'utf8'}
    )

    id = Column("idEMR", INTEGER, autoincrement=True, primary_key=True, nullable=False)  # pylint: disable=invalid-name
    DATA = Column(VARCHAR(666))
    User_idUser = Column(INTEGER, ForeignKey("User.idUser"), index=True, nullable=False)

    user = relationship("User", foreign_keys=[User_idUser], backref="emr")

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return "<Emr(%(id)s)>" % self.__dict__


class User(DECLARATIVE_BASE):

    __tablename__ = 'User'
    __table_args__ = (
        {'mysql_engine': 'InnoDB', 'sqlite_autoincrement': True, 'mysql_charset': 'utf8'}
    )

    id = Column(  # pylint: disable=invalid-name
        "idUser", INTEGER, autoincrement=True, primary_key=True, nullable=False
    )
    username = Column(VARCHAR(45))
    facebook_id = Column(VARCHAR(45))
    google_id = Column(VARCHAR(45))
    email = Column(VARCHAR(45))
    password = Column(VARCHAR(45))
    password_hash = Column(VARCHAR(45))
    email_confirmed = Column(VARCHAR(45))
    Usercol = Column(VARCHAR(45))

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return "<User(%(id)s)>" % self.__dict__


class Payment(DECLARATIVE_BASE):

    __tablename__ = 'Payments'
    __table_args__ = (
        {'mysql_engine': 'InnoDB', 'sqlite_autoincrement': True, 'mysql_charset': 'utf8'}
    )

    id = Column(  # pylint: disable=invalid-name
        "idPayments", INTEGER, autoincrement=True, primary_key=True, nullable=False
    )
    Payment_detail_columns = Column(VARCHAR(45))
    User_idUser = Column(INTEGER, ForeignKey("User.idUser"), index=True, nullable=False)

    user = relationship("User", foreign_keys=[User_idUser], backref="payments")

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return "<Payment(%(id)s)>" % self.__dict__


class Sub(DECLARATIVE_BASE):

    __tablename__ = 'Sub'
    __table_args__ = (
        {'mysql_engine': 'InnoDB', 'sqlite_autoincrement': True, 'mysql_charset': 'utf8'}
    )

    id = Column(  # pylint: disable=invalid-name
        "idSubs", INTEGER, autoincrement=True, primary_key=True, nullable=False
    )
    subdata = Column(VARCHAR(45))
    User_idUser = Column(INTEGER, ForeignKey("User.idUser"), index=True, nullable=False)

    user = relationship("User", foreign_keys=[User_idUser], backref="sub")

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return "<Sub(%(id)s)>" % self.__dict__


class Q(DECLARATIVE_BASE):

    __tablename__ = 'Q'
    __table_args__ = (
        {'mysql_engine': 'InnoDB', 'sqlite_autoincrement': True, 'mysql_charset': 'utf8'}
    )

    id = Column("idQ", INTEGER, autoincrement=True, primary_key=True, nullable=False)  # pylint: disable=invalid-name
    Text = Column(VARCHAR(45))
    EMR_idEMR = Column(INTEGER, ForeignKey("EMR.idEMR"), index=True, nullable=False)
    User_idUser = Column(INTEGER, ForeignKey("User.idUser"), index=True, nullable=False)

    emr = relationship("Emr", foreign_keys=[EMR_idEMR], backref="q")
    user = relationship("User", foreign_keys=[User_idUser], backref="q")

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return "<Q(%(id)s)>" % self.__dict__


class Role(DECLARATIVE_BASE):

    __tablename__ = 'Role'
    __table_args__ = (
        {'mysql_engine': 'InnoDB', 'sqlite_autoincrement': True, 'mysql_charset': 'utf8'}
    )

    id = Column(  # pylint: disable=invalid-name
        "idRole", INTEGER, autoincrement=True, primary_key=True, nullable=False
    )
    Role = Column(VARCHAR(45))

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return "<Role(%(id)s)>" % self.__dict__


class Userrole(DECLARATIVE_BASE):

    __tablename__ = 'UserRoles'
    __table_args__ = (
        {'mysql_engine': 'InnoDB', 'mysql_charset': 'utf8'}
    )

    id = Column("idUserRoles", INTEGER, primary_key=True, nullable=False)  # pylint: disable=invalid-name
    Role_idRole = Column(INTEGER, ForeignKey("Role.idRole"), index=True, nullable=False)
    User_idUser = Column(INTEGER, ForeignKey("User.idUser"), index=True, nullable=False)

    user = relationship("User", foreign_keys=[User_idUser], backref="userroles")
    role = relationship("Role", foreign_keys=[Role_idRole], backref="userroles")

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return "<Userrole(%(id)s)>" % self.__dict__


class Blogpost(DECLARATIVE_BASE):

    __tablename__ = 'BlogPost'
    __table_args__ = (
        {'mysql_engine': 'InnoDB', 'sqlite_autoincrement': True, 'mysql_charset': 'utf8'}
    )

    id = Column(  # pylint: disable=invalid-name
        "idBlogPost", INTEGER, autoincrement=True, primary_key=True, nullable=False
    )
    title = Column(VARCHAR(45))
    content = Column(VARCHAR(45))
    created_at = Column(DATETIME)
    author = Column(VARCHAR(45))
    tags = Column(VARCHAR(45))

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return "<Blogpost(%(id)s)>" % self.__dict__


class Blogcomment(DECLARATIVE_BASE):

    __tablename__ = 'BlogComments'
    __table_args__ = (
        {'mysql_engine': 'InnoDB', 'sqlite_autoincrement': True, 'mysql_charset': 'utf8'}
    )

    id = Column(INTEGER, autoincrement=True, primary_key=True, nullable=False)  # pylint: disable=invalid-name
    User_idUser = Column(INTEGER, ForeignKey("User.idUser"), index=True, nullable=False)
    BlogPost_idBlogPost = Column(INTEGER, ForeignKey("BlogPost.idBlogPost"), index=True, nullable=False)
    comment = Column(VARCHAR(450))

    user = relationship("User", foreign_keys=[User_idUser], backref="blogcomments")
    blogpost = relationship("Blogpost", foreign_keys=[BlogPost_idBlogPost], backref="blogcomments")

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return "<Blogcomment(%(id)s)>" % self.__dict__



engine = create_engine('sqlite:////home/foo.db')



