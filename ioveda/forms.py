from flask_wtf import Form
from wtforms.fields import StringField, PasswordField, BooleanField, SubmitField, IntegerField, DateField
from wtforms.fields import TextField
from flask.ext.wtf.html5 import URLField
from wtforms.validators import DataRequired, Length, Email, Regexp, EqualTo,\
    url, ValidationError

from models import User



from wtforms.widgets import TextArea

# class BookmarkForm(Form):
#     url = URLField('The URL for your bookmark:', validators=[DataRequired(), url()])
#     description = StringField('Add an optional description:')


#     def validate(self):
#         if not self.url.data.startswith("http://") or\
#             self.url.data.startswith("https://"):
#             self.url.data = "http://" + self.url.data

#         if not Form.validate(self):
#             return False

#         if not self.description.data:
#             self.description.data = self.url.data

#         # return True

class EMRForm(Form):
    user_birthdate = StringField('whateva:')
    user_gender = StringField('whateva:')
    user_height = StringField('whateva:')
    user_weight = StringField('whateva:')
    
    maincomplaints_symptom = StringField('whateva:')
    maincomplaints_others = StringField('whateva:')
    maincomplaints_condition = StringField('whateva:')
    maincomplaints_climatic = StringField('whateva:')
    maincomplaints_symptom_trigger = StringField('whateva:')
    maincomplaints_medicines = StringField('whateva:')
    
    medicalhistory_medicines = StringField('whateva:')
    medicalhistory_sideeffects = StringField('whateva:')
    medicalhistory_climatic = StringField('whateva:')
    medicalhistory_diseases = StringField('whateva:')
    medicalhistory_immune = StringField('whateva:')
    
    digestive_immune = StringField('whateva:')
    digestive_bowel = StringField('whateva:')
    digestive_wind = StringField('whateva:')
    digestive_acid = StringField('whateva:')
    digestive_heaviness = StringField('whateva:')
    
    urinary_burning = StringField('whateva:')
    urinary_color = StringField('whateva:')
    
    sleep_pattern = StringField('whateva:')
    sleep_dream = StringField('whateva:')
    
    psy_emotion = StringField('whateva:')
    psy_emotion_other = StringField('whateva:')
    psy_angry = StringField('whateva:')
    
    habit_smoke = StringField('whateva:')
    habit_alcohol = StringField('whateva:')
    
    
    female_menstruation = StringField('whateva:')
    female_flow = StringField('whateva:')
    female_symptoms = StringField('whateva:')
    female_kids = StringField('whateva:')
    female_delivery = StringField('whateva:')





class LoginForm(Form):
    username = StringField('username', validators=[DataRequired()])
    password = PasswordField('password', validators=[DataRequired()])
    remember_me = BooleanField('remember')
    submit = SubmitField('Log In')


class SignupForm(Form):
    username = StringField('Username',
                    validators=[
                        DataRequired(), Length(3, 80),
                        Regexp('^[A-Za-z0-9_]{3,}$',
                            message='Usernames consist of numbers, letters,'
                                    'and underscores.')
                                    ])
                                    
                                    
    password = PasswordField('Password',
                    validators=[
                        DataRequired(),
                        EqualTo('password2', message='Passwords must match.')
                        ])
            
    password2 = PasswordField('Confirm password', validators=[DataRequired()])
    
    email = StringField('Email',
                        validators=[DataRequired(), Length(1, 120), Email()])

    def validate_email(self, email_field):
        if User.query.filter_by(email=email_field.data).first():
            raise ValidationError('There already is a user with this email address.')

    def validate_username(self, username_field):
        if User.query.filter_by(username=username_field.data).first():
            raise ValidationError('This username is already taken.')
    
            
class ChangePasswordForm(Form):
    newpassword = PasswordField(validators=[DataRequired(),
                                         EqualTo('newpassword2',message='psw dosent match')])
                                         
    newpassword2 = PasswordField(validators=[DataRequired()])

class RequestPasswordChange(Form):
    email = StringField('Email', validators=[DataRequired(), Email()])
    
    
class BlogPost(Form):
    title = StringField('post title', 
                        widget=TextArea(), validators=[DataRequired()])
                        
    text = StringField('post text', widget=TextArea())
    

    
    



    



