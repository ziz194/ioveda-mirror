from flask import render_template, flash, redirect, url_for, request, session, render_template_string, g
from flask_login import login_required, login_user, logout_user, current_user
from flask import make_response
from ioveda import (app, db, login_manager, mandrill, csrf, safe, 
                   facebook, google, babel, paypalrestsdk, 
                   paypal_config, stripe)

from forms import ( EMRForm, LoginForm, SignupForm, ChangePasswordForm, 
                   RequestPasswordChange, BlogPost )

from models import *



#import payment configs

#plans
from ioveda import Plan, plan1, plan2, plan3


import gc

import json
import pandas as pd
import datetime
import sys

from helper import n_months_from_now
from ioveda import geoip
reload(sys)
sys.setdefaultencoding("utf-8")



def get_Plan_instance_by_id(ix):
    for obj in gc.get_objects():
        if isinstance(obj, Plan) and  obj.ix == ix:
            return obj
import gc           
def get_Plan_by_paypal_description(description):
    for obj in gc.get_objects():
        if isinstance(obj, Plan) and obj.description == description:
            return obj
            

#-----------------------------------------CONFIG-----------------------------

@babel.localeselector
def get_locale():
    return request.accept_languages.best_match(['de','en','pl','us'])


def user_location():
    ip = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
    return geoip.country_code_by_addr(ip)
    

@facebook.tokengetter
def get_facebook_oauth_token():
    return session.get('oauth_token') 
    
@login_manager.user_loader
def load_user(userid):
    return User.query.get(int(userid))

def current_user_id():
    return str(current_user.id)
    
    
#---------------------------------------/ CONFIG-----------------------      ---------    
    
    
    
#---------------------------------------MAIN VIEWS-----------------------------------

@app.route('/location')
def location():
    return user_location()
    

    
@app.route('/')
@app.route('/index')
def index():    
    response = make_response(render_template('index.html', 
                                             plan1 = plan1.plans_page_params(),
                                             plan2 = plan2.plans_page_params(),
                                             plan3 = plan3.plans_page_params()
                                             )
                            ) 
                                                                                
    try:
        response.set_cookie('api_token',value=current_user.generate_auth_token())
    except AttributeError:
        response.set_cookie('api_token',value='user_not_authenticated')        
    return response                               
                                           
@app.route('/emrform', methods=['GET', 'POST'])
@login_required
@csrf.exempt
def emrform():
    if request.method == 'POST': 
        form = EMRForm(csrf_enabled=False)
        bm = EMR(user=current_user,**form.data)              
        db.session.add(bm)
        db.session.commit()
        return redirect(url_for('plans_page'))
    else:
        return render_template('emr.html')


@app.route('/payments')
@login_required
def payments_page():
    plan = request.args.get('plan','2')
    current_plan = get_Plan_instance_by_id(plan)        
    params = current_plan.payments_page_params()          
    return render_template('payments.html',pp=params)



        
@login_required
@app.route('/plans')
def plans_page():
      
    return render_template('plans.html', plan1 = plan1.plans_page_params(),
                                         plan2 = plan2.plans_page_params(),
                                         plan3 = plan3.plans_page_params()
                                         )
@app.route('/order')
def order_page():
    
    #set default plan 'standard_yearly' on /order
    plan = request.args.get('plan','2')
    pkey = app.config['STRIPE_PUBLISHABLE_KEY']
    s = '/payments/stripe/plan/{plan}'.format(plan=plan)
    return render_template('order.html',pkey=pkey, stripe_url=s)
    
    
    

#--------------------------------/MAIN VIEWS----------------------------------------

#-------------------------------- USER AND LOGIN------------------------------------

@app.route('/user/change_password',methods=['GET','POST'])
@login_required
@csrf.exempt
def change_password():
    if request.method == 'POST':
        changeform = ChangePasswordForm(csrf_enabled=False)        
        if changeform.validate_on_submit():
            user = User.get_by_username(current_user.username)
            user.password = changeform.newpassword.data
            db.session.commit()
            return 'success'      
        else:
            changeform.validate()
            return json.dumps(changeform.errors)
    else: 
        return "some form html file"

@csrf.exempt 
@app.route("/login", methods=['POST','GET'])
def login():
    nex = request.args.get('next','') 
    nex = request.args.get('next')
    session['next'] = nex
    form = LoginForm()
    if request.method == 'POST' and form.validate():
        if '@' in form.username.data:
            user = User.get_by_email(form.username.data)
        else:
            user = User.get_by_username(form.username.data)
            
        if user is not None  and user.check_password(form.password.data) \
        and form.validate():
            login_user(user, form.remember_me.data)
            return json.dumps({"success":True,
                               "redirect":nex,
                               "token":current_user.generate_auth_token()})
        else:
            form.validate()
            err = form.errors
            if user:
                if not user.check_password(form.password.data):                
                    err['password'] = "invalid password"
            else:
                err['user'] = 'user not in database'
            return json.dumps(err)
    else:
        return render_template("signin.html")
    
@app.route("/logout")
def logout():
    logout_user()
    return redirect(url_for('index'))

@app.route("/signup", methods=["POST","GET"])
def signup():
    if request.method == 'POST':
        form = SignupForm()
        if form.validate_on_submit():
            user = User(email=form.email.data,
                        username=form.username.data,
                        password = form.password.data)
            db.session.add(user)
            db.session.commit()
            token = safe.dumps(form.email.data)
            send_signup_email(form.username.data, form.email.data, token)                                               
            return json.dumps({"success":True})
        else:
            form.validate()
            return json.dumps(form.errors)
    else:
        return render_template("signup.html")
        
@app.route('/user/confirm_email/<token>')
def confirm_email(token):
    email_from_token = safe.loads(token)
    q = User.query.filter_by(email=email_from_token).first()
    q.email_confirmed = True
    db.session.commit()
    return redirect(url_for('index'))

@app.route('/user/reset_password', methods=["POST","GET"])
@csrf.exempt
def reset_password(): 
    form = RequestPasswordChange(csrf_context=False)
    if request.method == 'POST':
        if form.validate_on_submit():
                if User.get_by_email(form.email.data):
                    token = safe.dumps(form.email.data)
                    mandrill.send_email(from_email='ioveda@ioveda.net',
                    to=[{'email':form.email.data}],
                        text='http://localhost:5000/token_login/'+token
                        ) 
                    return 'success'
                else:
                    return json.dumps({"email":"provided email adress not in database"})    
                    
        form.validate()
        return json.dumps(form.errors)                
    else:
        return "some html file"
            
                
    if not user:
        return json.dumps({"email":"user not in database"})
        

@app.route('/user/token_login/<token>', methods=['GET','POST'])
@csrf.exempt
def change_password_token(token):
    email = safe.loads(token)
    user = User.get_by_email(email)
    if user: 
        login_user(user, False)
        return redirect('/user/change_password')
    
@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 44

@app.errorhandler(405)
def method_not_allowed(e):
    return redirect(url_for('index'))
    

    
@app.route('/login2')
def login2():
    return facebook.authorize(callback=url_for('facebook_authorized',
        next=request.args.get('next') or request.referrer or None,
        _external=True))
    

@app.route('/login2/authorized')
@facebook.authorized_handler
def facebook_authorized(resp):
    if resp is None:
        return 'Access denied: reason=%s error=%s' % (
            request.args['error_reason'],
            request.args['error_description']
        ) 
    session['oauth_token'] = (resp['access_token'], '')
    me = facebook.get('/me')
    fb_id = me.data['id']
    username = me.data['name']
    email = me.data['email']
#    return str(me.data) {u'first_name': u'Jakub', u'last_name': u'Cieslik', u'verified': True, u'name': u'Jakub Cieslik', u'locale': u'en_US', u'gender': u'male', u'email': u'kubacieslik@gmail.com', u'link': u'https://www.facebook.com/app_scoped_user_id/10206815881895171/', u'timezone': 2, u'updated_time': u'2015-06-05T01:36:18+0000', u'id': u'10206815881895171'}
    fb_User = User(username=username, facebook_id=fb_id, email=email)
    if not User.query.filter_by(facebook_id = fb_id).first():  
        db.session.add(fb_User)
        db.session.commit()
        token = safe.dumps(email)
        send_signup_email(username, email, token)     
    
    login_user(User.get_by_fb(fb_id))
    session['loged_in'] = True
    
    return redirect(url_for('index'))


@app.route('/googlelogin')
def googlelogin():
    callback=url_for('authorized', _external=True)
    return google.authorize(callback=callback)

@app.route(app.config['GOOGLE_REDIRECT_URL'])
@google.authorized_handler
def authorized(resp):
    access_token = resp['access_token']
    session['access_token'] = access_token, ''
        
    import requests     
    headers = {'Authorization': 'OAuth '+access_token}
    req = requests.get('https://www.googleapis.com/oauth2/v1/userinfo', headers=headers)
    d = json.loads(req.text)
    guser = User(username=d['name'], email=d['email'], google_id=d['id'])
    
    if not User.get_by_google(d['id']):
        db.session.add(guser)
        db.session.commit()
        token = safe.dumps(d['email'])
        send_signup_email(d['name'], d['email'], token)
        
    login_user(User.get_by_google(d['id']))    
    return redirect(url_for('index'))


#--------------------------------/USER AND LOGIN-----------------------------------





#----------------------------------USER PROFILE----------------------------------

@login_required                          
@app.route('/profile')
def userpanel():
    g.user_status = current_user_status(int(current_user.id))
    date = str(datetime.datetime.utcnow().date())
    #questions = Questions.get_question_feed(current_user.id)
    return render_template('userpanel.html',date = date)    
     
@login_required
@app.route('/profile/use_coupon')
def cos():
    pass
@login_required
@app.route('/profile/account')
def user_account():
    d = user_account_data(current_user)
    return render_template('account.html', d=d)

@app.route('/profile/add_question',methods=['GET','POST'])
def add_question():  
    if request.method == 'GET':    
        return render_template('askquestion.html')
    else:
        comment = str(request.form['comment'])
        q = Questions(user = current_user, question_text = comment)
        db.session.add(q)
        db.session.commit()
        return "success"

@login_required     
@app.route('/profile/journey',methods=['GET','POST'])
def profile_journey():
    if request.method == 'GET':
        qs = journey_stream(current_user)
        return render_template('profile_journey.html', qs = qs)
    else:
        comment = str(request.form['comment'])
        q = Questions(user = current_user, question_text = comment)
        db.session.add(q)
        db.session.commit()
        return redirect(url_for('profile_journey'))



        

#-------------------------------PAYMENTS STRIPE-------------------------------
@login_required
@app.route('/payments/stripe/plan/<plan_id>', methods=['POST'])
def stripe_payment_plan(plan_id):
    current_plan = get_Plan_instance_by_id(plan_id)
    token = request.form['stripeToken']
    
    #create the subscription reccuring charge for user
    if not current_plan.one_time:
        try: customer = stripe.Customer.create(source=token, plan=current_plan.stripe_plan_name) 
        except Exception as e:
            flash(str(e))
            return redirect('order?plan={plan_id}'.format(plan_id=plan_id))
                
        subscription = Payments(user_id = current_user.id, 
                                customer_id = customer.id,
                                stripe_token = token, 
                                subscription_untill = n_months_from_now(current_plan.subscription_untill),
                                date = datetime.datetime.utcnow(), 
                                payment_gateway = 'stripe')
        db.session.add(subscription)
        db.session.commit()
        return 'success'
                
    if current_plan.one_time:
        try: charge = stripe.Charge.create(amount = current_plan.price * 100,
                                           currency = current_plan.currency.upper(),
                                           source = token,
                                           description = current_plan.description)
        except Exception as e:
            flash(str(e))
            return redirect('order?plan={plan_id}'.format(plan_id=plan_id))
        

        subscription = Payments(user_id = current_user.id, 
                                amount = str(current_plan.price),
                                charge_id = charge.id,
                                stripe_token = token, 
                                subscription_untill = n_months_from_now(current_plan.subscription_untill),
                                payment_gateway = 'stripe')
        db.session.add(subscription)
        db.session.commit()                     
        return 'success'
                        
                            
                                   
    db.session.add(subscription)    
    db.session.commit()
    
    return 'success'
    


@login_required
@app.route('/payments/stripe/plan2', methods=['POST'])
def subscribe_standard_year():
    plan_id = "ioveda_standard"    
    token = request.form['stripeToken']
    customer = stripe.Customer.create(source=token, plan=plan_id) 
    try: customer = stripe.Customer.create(source=token, plan=plan_id) 
    except Exception as e:
        flash(str(e))
        return redirect('order?plan=standard_yearly')
    


    subscription = Payments(user_id = current_user.id, customer_id = customer.id,
                       stripe_token = token)
                       
    send_subscription_email()                                              
    db.session.add(subscription)   
    db.session.commit()
    
    return redirect(url_for('userpanel'))
               
@login_required
@app.route('/payments/stripe/plan3', methods=['POST'])
def subscribe_premium():
    plan_id = 'ioveda_premium'
    token = request.form['stripeToken']
    customer = stripe.Customer.create(source=token,plan=plan_id)
    try: customer = stripe.Customer.create(source=token, plan=plan_id) 
    except Exception as e:
        flash(str(e))
        return redirect('order?plan=pro')

    subscription = Payments(user_id = current_user.id, customer_id = customer.id,
                       stripe_token = token
                    )
                    
    send_subscription_email()                             
    db.session.add(subscription)   
    db.session.commit()
    
    return redirect(url_for('userpanel'))
             
      
@login_required
@app.route('/payments/subscribe_premium_year', methods=['POST'])
def subscribe_premium_year():
    plan_id = "ioveda_premium_year"        
    token = request.form['stripeToken']
    try: customer = stripe.Customer.create(source=token, plan=plan_id) 
    except Exception as e:
        flash(str(e))
        return redirect('order?plan=pro')
        
    subscription = Payments(user_id = current_user.id, customer_id = customer.id,
                       stripe_token = token
                    )
                    
    send_subscription_email()                             
    db.session.add(subscription)    
    db.session.commit()
    
    return redirect(url_for('userpanel'))
    

#-------------------------------PAYMENTS PAYPAL-------------------------------
           
@app.route('/payments/paypal/plan/<plan_id>')
@login_required
def paypal_payment(plan_id):
    
    #find the right plan instance 
    current_plan = get_Plan_instance_by_id(plan_id) 
    payment = paypalrestsdk.Payment(current_plan.paypal_payment_config())
    payment.create()
    p = payment.to_dict()
    link = p['links'][1]['href']

    return redirect(link)
    
#@app.route('/payments/paypal_standard_yearly')
#@login_required
#def paypal_standard_yearly():
#    payment = paypalrestsdk.Payment(pp_standard_yearly)
#    payment.create()
#    p = payment.to_dict()
#    link = p['links'][1]['href']
#
#    return redirect(link)
#
#
#@app.route('/payments/paypal_pro')
#@login_required
#def paypal_pro():
#    payment = paypalrestsdk.Payment(pp_pro)
#    payment.create()
#    p = payment.to_dict()
#    link = p['links'][1]['href']
#
#    return redirect(link)
        
                                                                  
@app.route('/payments/paypal_success')
def paypal_success():
    
    payment_id = request.args.get('paymentId','')  
#    token = request.args.get('token','')  
    payer_id = request.args.get('PayerID','')
    payment = paypalrestsdk.Payment.find(payment_id)
    payment.execute({"payer_id": payer_id})     
    description = payment['transactions'][0]['description']
    
    current_plan = get_Plan_by_paypal_description(description)  
    p = Payments(user=current_user,
                 subscription_untill=n_months_from_now(current_plan.subscription_untill),
                 paypal_object = json.dumps(payment.to_dict()),
                 payment_type = payment_type,
                 payment_gateway = 'paypal',
                 charge_id = payment_id,
                 amount = str(current_plan.price)
                 )

                
    send_subscription_email()            
    db.session.add(p)
    db.session.commit()       
    flash('Your paypal payment was successfull')
    return redirect(url_for('userpanel'))
    
       
            
@app.route('/payments/paypal_failed')
def paypal_failed():
    error = 'Payment process was interrupted please try again.'
    flash(error)
    return redirect(url_for('plans_page'))





#---------------------------------BLOG STUFF-----------------------

    

@app.route('/blog')
@app.route('/blog/<int:ix>')
def blog_temp(ix=0):
    if ix == 0:
        blog_stream = BlogPost.blog_stream()
        return render_template('blog_all.html',blog_stream=blog_stream)
    else:
        blog_stream = [BlogPost.blog_article(ix)]
        return render_template('blog_all.html',blog_stream=blog_stream)
    



#-------------------------------DOCTOR PANEL----------------------------------------
@app.route('/doctor/',methods=['GET'])
@app.route('/doctor/<userid>',methods=['POST','GET'])
def doctor(userid=''):
    if request.method == 'GET':
        stream = doctor_stream()
        if userid:
            user_question = Questions.get_question_feed(int(userid), 10)
        else:
            user_question = ''           
        return render_template('doctor1.html',
                               users=stream,
                               user_question=user_question,
                               userid=userid,
                               user_needs_anwser= lambda x: user_needs_anwser(x))
    else:
        anwser = str(request.form['anwser'])
        anwser = Questions(user_id = int(userid), 
                           question_text = anwser,
                           doctor_id = 666)
        db.session.add(anwser)
        db.session.commit()
        return redirect('/doctor/'+userid)
        
 
                  
@login_required   
@app.route('/doctor/add_anwser/<userid>',methods=['POST'])
def doctor_anwser(userid):
    if request.method == 'POST':
        anwser = str(request.form['anwser'])
        anwser = Questions(user_id = int(userid), 
                           question_text = anwser,
                           doctor_id = 666)
        db.session.add(anwser)
        db.session.commit()
        return redirect('/doctor/'+userid)

#---------------------------------/END DOCTOR----------------------------------------





#------------------------------MISC-------------------------------------------------

@app.route('/book')
def book():
    return app.send_static_file('EverydayAyurvedBook.pdf')


@app.route('/terms_and_conditions')
def terms_and_conditions():
    return app.send_static_file('ioveda_terms_and_conditions.pdf')

@app.route('/product_details')
def product_details():
    return render_template('product_details.html')
    
    
@app.route('/sendemail')
def send_email():
    send_signup_email('jc', 'kubacieslik@gmail.com', 'http://djkldjdkljdkljdkld')
    send_subscription_email()
    return 'succsess'

@app.route('/render/<template>')
def render_temp(template):
        return render_template(template)



#---------------------------------------MAILING----------------------------------
def send_signup_email(name, email, token):
    confirm_url = 'http://ioveda.net/' + 'confirm_email/' + token  
    
    text = open('ioveda/templates/signup_email.txt').read()
    email_template = text.format(name = name, token = confirm_url)    
    
    mandrill.send_email(from_email='shreyasgite@ioveda.net',
                    to=[{'email':email}],                        
                        subject = 'Greetings from team ioveda',
                        text = email_template,
                        track_opens = True,
                        track_click = True
                        )
                        
    return 'succsess'
    

def send_subscription_email():
    name = current_user.username
    email = current_user.email
    text = open('ioveda/templates/subscription_email.txt').read()
    text = text.format(name=name)
    mandrill.send_email(from_email='support@ioveda.net',
                        to=[{'email':email}],
                            subject = 'ioveda subscription',
                            text = text,
                            track_opens = True,
                            track_clicks = True)
    return "success"
    
    

 
#--------------------------------------END MAILING-------------------------------   

    
      
#------------------------------------ADMIN STUFF----------------------------------
@app.route('/db/<table>')
def inspect_db(table):
    if current_user.is_authenticated() and current_user.username == 'admin' or \
    current_user.email == 'kubacieslik@gmail.com':
        return pd.read_sql(table, db.engine).transpose().to_html()
    else: return redirect(url_for('login'))




#------------------------------------END AMIN-------------------------------------

#-----------------------random testin------------------------
@app.route('/blog/<id>')
def blog_perma(id):
    
    article = BlogPost.blog_article(int(id))
    return render_template('BBlog.html',art=article)

@app.route('/user/profile_details')
@login_required
def profile_details():
    pass

@app.route('/okokok/okokok/urlfor')
def urlurl():
    return render_template_string('''{{ url_for('static',filename='faq.css') }}''')
    


#----------------------------- USERPANEL API ---------------------
#{'action':'get_emr',
#'token':'sometoken'}
@app.route('/api/user',methods=['POST'])
def user_api():
    token = request.json.get('token')
    action = request.json.get('action')
    user = User.verify_auth_token(token)
    if not user:
        return json.dumps({'success': False, 'messege': 'invalid token'
                          })

    elif action == 'get_emr':
        return json.dumps({'success': True,
                          'data': EMR.serialized_emr(user_id=user.id)})
                          
    elif action == 'get_history':
        return json.dumps({'success': True,
                          'data': Questions.serialized_history_feed(user_id=user.id,
                          limes=1000)})
                          
    elif action == 'post_question':
        r = request.json
        q = r['data']['question']
        question = Questions(user=user, question_text = q)
        db.session.add(question)
        db.session.commit()
        return json.dumps({'success':True,
                           'messege':'question saved'})
                                                                            
    else:
        return json.dumps({'success': False})
    
#    
#data = {'action':'post_question', 'token': token,
#        'data':{'question':'question_text'}}
#
#        
    
@app.route('/api/token',methods=['GET'])
@login_required
def give_token():
    response = make_response(render_template_string('''{0}'''.format(current_user.generate_auth_token())))
    response.set_cookie('api_token',value=current_user.generate_auth_token())
    return response
    
    

    
    

    
    
#
#













#@app.route('/path')
#def path():
#    return render_template_string(''' Hi the url of this file is \n
#    {{ request.path }} ''')
#
#    
#@app.route('/userpayments/<int:ix>')
#def pay(ix):
#    return json.dumps(get_user_payments(ix))
#    
#
#@app.route('/current_user')
#def curr():
#    return render_template_string(''' {{ current_user.id }} ''')
#    
#
#@app.route('/session1')
#def ss():
#    session['ok'] = 'okkkk'
#    return 'ok'
#
#@app.route('/session2')
#def sss():
#    return render_template_string('''{{ session['ok'] }}''',session=session)
#    