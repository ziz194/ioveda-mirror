from datetime import datetime
from itsdangerous import (TimedJSONWebSignatureSerializer as Serializer, BadSignature, SignatureExpired)
from flask_login import UserMixin
from werkzeug.security import check_password_hash, generate_password_hash
from ioveda import db
import json



keys_to_remove = ['_sa_instance_state','date_registered','date','user_id','id']

def remove_key(d, keys_to_remove=['_sa_instance_state','date_registered','date','user_id','id']):    
    r = dict(d)
    for rem_key in keys_to_remove:
        if rem_key in r.keys():
            del r[rem_key]
        else:
            print 'one or more keys to be removed were not found in dict'
    return r
    

class EMR(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)

    user_birthdate = db.Column(db.String(255))
    user_gender = db.Column(db.String(255))
    user_height = db.Column(db.String(255))
    user_weight = db.Column(db.String(255))
    
    maincomplaints_symptom = db.Column(db.String(255))
    maincomplaints_others = db.Column(db.String(255))
    maincomplaints_condition = db.Column(db.String(100))
    maincomplaints_climatic = db.Column(db.String(100))
    maincomplaints_symptom_trigger = db.Column(db.String(255))
    maincomplaints_medicines = db.Column(db.String(255))
        
    medicalhistory_medicines = db.Column(db.String(255))
    medicalhistory_sideeffects = db.Column(db.String(255))
    medicalhistory_climatic = db.Column(db.String(255))
    medicalhistory_diseases = db.Column(db.String(255))
    medicalhistory_immune = db.Column(db.String(255))

    digestive_immune = db.Column(db.String(255))
    digestive_bowel = db.Column(db.String(255))
    digestive_wind = db.Column(db.String(255))
    digestive_acid = db.Column(db.String(255))
    digestive_heaviness = db.Column(db.String(255))
    
    urinary_burning = db.Column(db.String(255))
    urinary_color = db.Column(db.String(255))
        
        
    sleep_pattern = db.Column(db.String(255))
    sleep_dream = db.Column(db.String(255))
    
    psy_emotion = db.Column(db.String(255))
    psy_emotion_other = db.Column(db.String(255))
    psy_angry = db.Column(db.String(255))
    
    habit_smoke = db.Column(db.String(255))
    habit_alcohol = db.Column(db.String(255))
    
    
    female_menstruation = db.Column(db.String(255))
    female_flow = db.Column(db.String(255))
    female_symptoms = db.Column(db.String(255))
    female_kids = db.Column(db.String(255))
    female_delivery = db.Column(db.String(255))
    
    
    date = db.Column(db.DateTime, default=datetime.utcnow)
    
    
    @staticmethod
    def serialized_emr(user_id):
        emr = db.session.query(EMR).filter_by(user_id = user_id).first()
        to_dict = remove_key(emr.__dict__, keys_to_remove)
        return to_dict
    

    #@staticmethod
    #def newest(num):
    #    return EMR.query.order_by(desc(EMR.date)).limit(num)

    #def __repr__(self):
     #   return "<EMR '{}': '{}'>".format(self.description, self.url)

class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80))
    facebook_id = db.Column(db.String(255), unique=True)
    google_id = db.Column(db.String(255))
    email = db.Column(db.String(120))
    password_hash = db.Column(db.String(255))
    email_confirmed = db.Column(db.Boolean, default=False)
    admin = db.Column(db.Boolean, default=False)
    sex = db.Column(db.String(255))
    
    emr = db.relationship('EMR', backref='user', lazy='dynamic')
    payments = db.relationship('Payments',backref='user',lazy='dynamic')
    question = db.relationship('Questions',backref='user',lazy='dynamic')
    posts = db.relationship('Post',backref='user', lazy='dynamic')
#    comments = db.relationship('BlogComments',backref='user',lazy='dynamic')
    
    roles = db.relationship('Role', secondary='user_roles',
                            backref=db.backref('user', lazy='dynamic'))
    date_registered = db.Column(db.DateTime, default=datetime.utcnow)

    
    
    def generate_auth_token(self, expiration = 600):
        s = Serializer('okokok', expires_in = expiration)
        return s.dumps({ 'id': self.id })
    
    @staticmethod
    def verify_auth_token(token):
        s = Serializer('okokok')
        try:
            data = s.loads(token)
        except SignatureExpired:
            return None # valid token, but expired
        except BadSignature:
            return None # invalid token
        user = User.query.get(data['id'])
        return user
        
    
    @property
    def password(self):
        raise AttributeError('password: write-only field')
        
    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)
    
    def check_password(self, password):
        return check_password_hash(self.password_hash, password)
    
    @staticmethod
    def get_by_username(username):
        return User.query.filter_by(username=username).first()
        
    @staticmethod
    def get_by_fb(fb_id):
        return User.query.filter_by(facebook_id=fb_id).first()
        
    @staticmethod
    def get_by_google(google_id):
        return User.query.filter_by(google_id=google_id).first()
        
    @staticmethod
    def get_by_email(email):
        return User.query.filter_by(email=email).first()
    
    @staticmethod 
    def is_admin(id):
        return User.query.filter_by(id=id).admin

    def __repr__(self):
        return "<User '{}'>".format(self.username)
        



class UserRoles(db.Model): 
    id = db.Column(db.Integer(), primary_key=True)  
    user_id = db.Column(db.Integer(), db.ForeignKey('user.id', ondelete='CASCADE'))
    role_id = db.Column(db.Integer(), db.ForeignKey('role.id', ondelete='CASCADE'))


class Role(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(50), unique=True)



class BlogPost(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(255), unique=True)
    text = db.Column(db.Text)
    author = db.Column(db.String(255))  
    date = db.Column(db.DateTime,default=datetime.utcnow)
    
    @staticmethod
    def blog_stream():
        return db.session.query(BlogPost).all()
    @staticmethod    
    def blog_article(ix):
        return db.session.query(BlogPost).filter_by(id=ix).first()
        
#    comment = db.relationship('BlogComments',backref='blogpost', lazy='dynamic')
#    
#    
#class BlogComments(db.Model):
#    id = db.Column(db.Integer, primary_key=True)
#    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
#    post_id  = db.Column(db.Integer, db.ForeignKey('blogpost.id'), nullable=False)
#    comment = db.Column(db.String(255))
   
class Payments(db.Model):
    id = db.Column(db.Integer, primary_key=True)    
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False) 
    amount = db.Column(db.String(255))
    charge_id = db.Column(db.String(255))
    customer_id = db.Column(db.String(255))
    stripe_token = db.Column(db.String(255))
    subscription_untill = db.Column(db.DateTime())
    payment_gateway = db.Column(db.String(255))
    payment_type = db.Column(db.String(255))
    paypal_object = db.Column(db.String(255))
    date = db.Column(db.DateTime, default=datetime.utcnow)
    
    


class Questions(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    doctor_id = db.Column(db.Integer, nullable=True)
    question_text = db.Column(db.String(255))
    date = db.Column(db.DateTime, default=datetime.utcnow)
    
    @staticmethod
    def get_question_feed(user_id, limes):      
        return reversed(db.session.query(Questions).
                        filter_by(user_id = user_id).
                        order_by(Questions.date.desc()).
                        limit(limes).\
                        all())
    
    @staticmethod
    def serialized_history_feed(user_id, limes):
        mess = reversed(db.session.query(Questions).
                        filter_by(user_id = user_id).
                        order_by(Questions.date.desc()).
                        limit(limes).
                        all())
        L = []                
        for q in mess:
            q = q.__dict__
            del q['_sa_instance_state']
            q['date'] = q['date'].isoformat()
            L.append(q)        
        return L
        
        

            
        
       
        
    
                    
                    

class Post(db.Model) :
    id = db.Column(db.Integer, primary_key=True)
    body = db.Column(db.Text)
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    author_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    
    
class Comments(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    body = db.Column(db.Text)
    date = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    author_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    post_id = db.Column(db.Integer, db.ForeignKey('post.id'))
    
    

def get_records_by_id(ix):
    return db.session.query(EMR).join(User).filter(User.id == ix).first()
    
    
def get_user_payments(ix):
    k = db.session.query(Payments).join(User).filter(User.id == ix).all()
    return str([b.amount for b in k])
    
def get_blog_post():
    return db.session.query(Post).join(User)
    
def current_user_status(id):
    if db.session.query(EMR).filter_by(user_id = id).first():
        emr = True
    else:emr = False    
    
    p = db.session.query(Payments).filter_by(user_id = id).first()
    if p:
        if p.subscription_untill > datetime.utcnow():
            paid = True
        else:
            paid = False     
        return {"valid_subscription":paid,"emr": emr}


def current_user_profile_details(id):
    if db.session.query(EMR).filter_by(user_id = id).first():
        emr = True
    else:emr = False    
    
    p = db.session.query(Payments).filter_by(user_id = id).first()
    if p:
        if p.subscription_untill > datetime.utcnow():
            paid = True
        else:
            paid = False     
        return {"valid_subscription":paid,"emr": emr}




def user_account_data(current_user):
    p = db.session.query(Payments).filter_by(user_id = current_user.id).first()
    if p:
        if p.subscription_untill > datetime.utcnow():
            untill = p.subscription_untill.strftime("%m-%d-%y")
            valid = True
        else:
            untill = ""
            valid = False
    else:
        valid=False
        untill = ''
               
    return {"subscription":{"valid":valid,
                            "untill":untill}}
        
                                                             
def journey_stream(current_user):
    qs=db.session.query(Questions).filter_by(user_id=current_user.id).order_by(Questions.date).all()
    return qs
    
def doctor_stream():
    users = db.session.query(User).all()
    return users
    


def user_needs_anwser(uid):
    r = db.session.query(Questions).filter_by(user_id=uid).order_by(Questions.date.desc()).first()
    if r:
        if r.doctor_id:
            return False
        else:
            return True
    else: return 'no_history_stream'
    
    

    
    
    

