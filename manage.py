#! /usr/bin/env python

from ioveda import app, db
from ioveda.models import User
from FlaskConfig import LiveConfig, DevelopementConfig
from flask.ext.script import Manager, prompt_bool, Server
from cherrypy import wsgiserver



d = wsgiserver.WSGIPathInfoDispatcher({'/': app})
server = wsgiserver.CherryPyWSGIServer(('0.0.0.0', 8080), d)

def cherry_server():
	try:
		server.start()
	except KeyboardInterrupt:
		server.stop()


manager = Manager(app)
manager.add_command("runserver", Server(host="0.0.0.0"))
#manager.add_command("cherrypy", cherry_server)


#initializes sqllite db 
@manager.command
def initdb():
    db.create_all()
    db.session.add(User(username="zxc", email="zxc@example.com", password="test"))
    db.session.add(User(username="admin", email="admin@ioveda.net", password="ioadmin"))
    db.session.add(User(username="zxczxc", email="zxzxcc@example.com", password="test"))
    db.session.commit()
    print 'Initialized the database'

@manager.command
def dropdb():
    if prompt_bool(
        "Are you sure you want to lose all your data"):
        db.drop_all()
        print 'Dropped the database'

if __name__ == '__main__':
	manager.run()
