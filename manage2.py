from cherrypy import wsgiserver
from ioveda import app





d = wsgiserver.WSGIPathInfoDispatcher({'/': app})
server = wsgiserver.CherryPyWSGIServer(('0.0.0.0', 8282), d)


if __name__ == '__main__':
   try:
      server.start()
   except KeyboardInterrupt:
      server.stop()

